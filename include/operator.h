//  _   _      _ _        _ ____  
// | \ | | ___(_) |_   _ (_)  _ \ 
// |  \| |/ _ \ | | | | || | | | |
// | |\  |  __/ | | |_| || | |_| |
// |_| \_|\___|_|_|\__,_|/ |____/ 
//                     |__/     
// Project name:    Sobel Filter
// Filename:        operator.h
// Creation Date :  23/04/2013

#ifndef SobelFilter_operator_h
#define SobelFilter_operator_h

//To use jpeg input image
#define cimg_use_jpeg 1

//To use png input image
#define cimg_use_png 1

#include "CImg.h"

typedef cimg_library::CImg<uint8_t> ImageUchar;

const int8_t dx8[9] = {-1,  0,  1, -1,  0,  1, -1,  0,  1};
const int8_t dy8[9] = {-1, -1, -1,  0,  0,  0,  1,  1,  1};

void rgb_to_grayscale(ImageUchar& img);
void sobel_filter(ImageUchar& img);
void gaussian_blur(ImageUchar& img, float half_size);

#endif