# Sobel Filter - by NeilujD #
## Description ##
The Sobel filter is a edge detection filter.
## How to install it ##
* Install git on your system.
* Clone this repository :

```
#!bash

git clone https://bitbucket.org/NeilujD/sobel-filter.git
```
* If not yet, install cmake on your system.
* Go in your cloned repository and the run the following commands :

```
#!bash

cmake CMakeLists.txt
make
./build/sobelfilter
```
## How to use it ##
1. Type the image input's path (by default use : ```resources/sobel_test_1.jpg``` or ```resources/sobel_test_2.png```)
2. Press enter and then the filtered image may appear. Also you can find the intermediate images (named like ```result_[operator].bmp``` in your project root's path) to see the different steps.
## How does it work ##
### Operators ###
1. First we transform the RGB input image in a grayscale image to simplify the few next step.
2. Then we add a gaussian filter to improve the edge detection on the grayscale image.

For more details please have a look on [my blog post](http://neilujd.com/what-is-an-image-filter/).

### Sobel operator ###
The Sobel filter has been developed helped by the following [Wikipedia reference](http://en.wikipedia.org/wiki/Sobel_operator).