//  _   _      _ _        _ ____  
// | \ | | ___(_) |_   _ (_)  _ \ 
// |  \| |/ _ \ | | | | || | | | |
// | |\  |  __/ | | |_| || | |_| |
// |_| \_|\___|_|_|\__,_|/ |____/ 
//                     |__/     
// Project name :   Sobel Filter
// Filename :       operator.cpp
// Creation Date :  23/04/2013

#include <stdint.h>
#include <cmath>
#include <iostream>

#include "operator.h"

// Transform a color image into grayscale image
// @param img [in, out] Image to transform
void rgb_to_grayscale(ImageUchar& img) {
  for (int y = 0; y < img.height(); ++y)
  {
    for (int x = 0; x < img.width(); ++x)
    {
      img(x, y, 0) = uint8_t(0.212671f * img(x, y, 0) + 
                             0.715160f * img(x, y, 1) +
                             0.072169f * img(x, y, 2));
    }
  }
  img.resize(-100, -100, -100, 1);
}

// Apply the Sobel filter to an image
// @param img [in, out] Image to transform
void sobel_filter(ImageUchar& img) {

  int32_t gx, gy;
  ImageUchar img_tmp(img);

  for (int y = 1; y < img.height() - 1; ++y)
  {
    for (int x = 1; x < img.width() - 1; ++x)
    {
      gx = ((1 * img(x + dx8[0], y + dy8[0])) + 
            (2 * img(x + dx8[3], y + dy8[3])) +
            (1 * img(x + dx8[6], y + dy8[6]))) -
           ((1 * img(x + dx8[2], y + dy8[2])) +
            (2 * img(x + dx8[5], y + dy8[5])) +
            (1 * img(x + dx8[8], y + dy8[8])));

      gy = ((1 * img(x + dx8[0], y + dy8[0])) +
            (2 * img(x + dx8[1], y + dy8[1])) +
            (1 * img(x + dx8[2], y + dy8[2]))) -
           ((1 * img(x + dx8[6], y + dy8[6])) +
            (2 * img(x + dx8[7], y + dy8[7])) +
            (1 * img(x + dx8[8], y + dy8[8])));

      img_tmp(x, y) = sqrt((gx * gx) + (gy * gy));
    }
  }

  img = img_tmp;
}

// Apply the blur gaussian effect from the CImg lib
// @param img [in, out] Image to transform
// @param sigma [in] sigma value to use in blur method
void gaussian_blur(ImageUchar& img, float sigma) {
  img.blur(sigma,sigma,sigma, true, false);
}