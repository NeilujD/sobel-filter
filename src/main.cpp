//  _   _      _ _        _ ____  
// | \ | | ___(_) |_   _ (_)  _ \ 
// |  \| |/ _ \ | | | | || | | | |
// | |\  |  __/ | | |_| || | |_| |
// |_| \_|\___|_|_|\__,_|/ |____/ 
//                     |__/     
// Project name:    Sobel Filter
// Filename:        main.cpp
// Creation Date :  23/04/2013

#include <iostream>

#include "operator.h"

int main(int, const char **)
{
  std::cout << "-----* Sobel Filter *-----" << std::endl;
  std::cout << "Name of the picture to test : ";

  std::string picture_name;
  std::getline (std::cin, picture_name);

  ImageUchar sobel_picture(picture_name.c_str());

  rgb_to_grayscale(sobel_picture); 
  sobel_picture.save_bmp("result_grayscale.bmp"); 
  gaussian_blur(sobel_picture, 3.0);
  sobel_picture.save_bmp("result_gaussian.bmp"); 
  sobel_filter(sobel_picture);

  sobel_picture.save_bmp("result_final.bmp");
  std::cout << "Result save in result_final.bmp" << std::endl;

  sobel_picture.display(0, false);

  return 0;
}